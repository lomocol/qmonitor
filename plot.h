#ifndef PLOT_H
#define PLOT_H

#include "graphic.h"

class Plot
{
public:
    Plot(QString ip, int Max, bool history = true);
    ~Plot();
    QVector<Graphic *> graphs;
    QCustomPlot *plot;
    QString ip;
    int max_key;
    int real_time;
    int graphs_count;
    long long int first;
    bool history;
    int CheckFirst();
    void AddGraph(QString title, QColor clr, QCPGraph::LineStyle ls,
                  int width = 3);
    int Check();
    void SetPen(int width = 8);
    void Update();
    void SetHistory();
    void SetData();
    void SetRange();

private:
    void CreatePlot(int Max);
};

#endif // PLOT_H
