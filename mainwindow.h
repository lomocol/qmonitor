#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "plot.h"
#include "table.h"
#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QTimer>
#include <chrono>

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void LoadKeys();
    void CreatePlots();
    void Draw();
    void SetPen(int width);
private slots:
    void PlotTick();
    void CreateNewConnection(const QString &new_ip);
    void DeleteOldConnection();
    void ConnectDoubleClick();
    void ChangeRealTime();
    void TableTick();
    void CheckFirst();
    void on_SetIpButton_clicked();
    void on_RealTimeButton_1_clicked();
    void on_RealTimeButton_2_clicked();

private:
    QVector<QColor> color;
    QVector<Plot *> plots;
    Table *table;
    QString ip;
    QVector<QString> keys;
    QVector<QString> cpu_keys;
    QVector<QString> mem_appl_keys;
    QVector<QString> cpu_appl_keys;
    QVector<QString> mem_total_keys;
    QVector<QGridLayout *> layouts;

    Ui::MainWindow *ui;
    Commander *commander;
};

#endif // MAINWINDOW_H
