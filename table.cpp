#include "table.h"

Table::Table(QTableWidget *_table, QString _ip)
{
    ip    = _ip;
    table = _table;
    table->clear();
    Restructure();
    commander = new Commander(ip);

    table->setColumnCount(1);
    table->setHorizontalHeaderItem(0, new QTableWidgetItem("score"));
    Upgrade();
}
void Table::Upgrade()
{
    QString command      = "keys hits.*";
    QVector<QString> tmp = commander->SendCommand(command);

    if (tmp.size() == keys.size()) {
        for (int i = 0; i < tmp.size(); i++)
            if (tmp[i] != keys[i]) {
                keys = tmp;
                Restructure();
                SetItems();
                return;
            }
        SetItems();
    } else {
        keys = tmp;
        Restructure();
        SetItems();
        return;
    }
}
void Table::Restructure()
{
    table->setRowCount(keys.size());
    items.clear();
    items.resize(keys.size());
    for (int i = 0; i < keys.size(); i++) {
        items[i] = new QTableWidgetItem();
        table->setVerticalHeaderItem(i, new QTableWidgetItem(keys[i]));
        table->setItem(i, 0, items[i]);
    }
}

void Table::SetItems()
{
    int c = 0;
    for (int i = 0; i < keys.size(); i++) {
        c = commander->GetHits("GET " + keys[i]);
        items[i]->setText(QString::number(c));
    }
}
Table::~Table()
{
    delete commander;
}
