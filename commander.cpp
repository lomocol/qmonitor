#include "commander.h"

Commander::Commander(QString _ip)
{
    ip                 = _ip;
    QByteArray ba      = ip.toLocal8Bit();
    const char *c_str2 = ba.data();
    connect            = redisConnect(c_str2, 6379);
    err                = connect->err;
    errstr             = connect->errstr;
    reply              = nullptr;
}

QVector<QString> &Commander::SendCommand(const QString &command)
{
    response.clear();

    bytes = command.toLocal8Bit();
    reply = static_cast<redisReply *>(redisCommand(connect, bytes.constData()));
    for (unsigned int i = 0; i < reply->elements; i++) {
        response.push_back(reply->element[i][0].str);
    }
    freeReplyObject(reply);
    return response;
}
int Commander::GetHits(const QString &command)
{
    bytes = command.toLocal8Bit();
    reply = static_cast<redisReply *>(redisCommand(connect, bytes.constData()));
    int rsp = atoi(reply->str);
    freeReplyObject(reply);
    return rsp;
}
long long Commander::Check(const QString &command)
{
    bytes = command.toLocal8Bit();
    reply = static_cast<redisReply *>(redisCommand(connect, bytes.constData()));
    long long rsp = reply->integer;
    freeReplyObject(reply);
    return rsp;
}

Commander::~Commander()
{
    redisFree(connect);
}
