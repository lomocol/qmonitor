#include "graphic.h"

Graphic::~Graphic()
{
    delete commander;
}
Graphic::Graphic(const QString &str, QColor clr, const QString &ip)
{
    first = 0;
    color = clr;
    line  = str;

    commander = new Commander(ip);
    count     = 0;

    Check();
    size    = static_cast<int>(count);
    records = QVector<QStringList>(size);
    keys    = QVector<double>(size, 0);
    values  = QVector<double>(size, 0);

    GetHistory();
}

void Graphic::SetData()
{
    graphic->addData(nextKey, nextVal);
}
void Graphic::Update()
{
    //        SendCommand("zrange " + line + " " + QString::number(count - 1) +
    //        " -1");
    SendCommand("zrange " + line + " -1 -1");
    nextKey = (records.back()[0].toDouble()) / DIV - first;
    nextVal = records.back()[1].split("M")[0].toDouble();
}
int Graphic::SendCommand(QString command)
{

    QVector<QString> &vtmp = commander->SendCommand(command);
    if (vtmp.size() > 0) {
        records.clear();
        for (int i = 0; i < vtmp.size(); i++) {
            records.push_back(vtmp[i].split(':'));
        }
        return vtmp.size();
    } else
        return -1;
}
int Graphic::Check()
{
    long long x = commander->Check("ZCOUNT " + line + " 0 1");

    if (x != count) {
        count = x;
        return 1;
    } else
        return -1;
}

void Graphic::GetHistory()
{

    SendCommand("zrange " + line + " 0 " + QString::number(count) + " ");
    for (int i = size - 1, j = static_cast<int>(count) - 1;
         i > size - count - 1; i--, j--) {
        values[i] = records[j][1].split("M")[0].toDouble();
        keys[i]   = (records[j][0].toDouble()) / DIV;
    }
    if (count > 0)
        first = static_cast<long long>(keys.back());
}
void Graphic::SetHistory()
{
    for (int i = 0; i < keys.size(); i++) {
        keys[i] -= first;
    }
    graphic->setData(keys, values);
    nextKey = keys.back();
    nextVal = values.back();
}
