#include "plot.h"

extern const int interval;
extern const int padding_right;
extern const int legend_font_size;
extern const int DIV;

Plot::Plot(QString _ip, int Max, bool _history)
{
    max_key      = -1;
    ip           = _ip;
    history      = _history;
    real_time    = true;
    graphs_count = 0;
    CreatePlot(Max);
}

void Plot::SetHistory()
{
    for (int i = 0; i < graphs_count; i++) {
        graphs[i]->SetHistory();
    }
}
void Plot::CreatePlot(int Max)
{
    plot = new QCustomPlot();

    plot->setInteraction(QCP::iRangeZoom, true);
    plot->setInteraction(QCP::iRangeDrag, true);
    plot->axisRect()->setRangeDrag(
        Qt::Horizontal); // Enable only drag along the horizontal axis
    plot->axisRect()->setRangeZoom(
        Qt::Horizontal); // Enable zoom only on the horizontal axis
    plot->xAxis->setTickLabelType(
        QCPAxis::ltNumber); // Labl coordinates along the X axis as the Date and
                            // Time
    plot->legend->setTextColor(Qt::red);
    // Customizable fonts on the axes
    plot->xAxis->setTickLabelFont(QFont(QFont().family(), 8));
    plot->yAxis->setTickLabelFont(QFont(QFont().family(), 8));
    plot->yAxis2->setTickLabelFont(QFont(QFont().family(), 8));
    // Automatic scaling ticks on the X-axis
    plot->xAxis->setAutoTickStep(true);

    /* We are making visible the X-axis and Y on the top and right edges of the
     * graph, but disable them tick and labels coordinates
     * */
    plot->xAxis2->setVisible(true);
    plot->yAxis2->setVisible(true);
    plot->xAxis2->setTicks(false);
    plot->yAxis2->setTicks(true);

    plot->xAxis2->setTickLabels(false);
    plot->yAxis2->setTickLabels(true);

    plot->yAxis->setTickLabelColor(QColor(Qt::red));
    plot->yAxis2->setTickLabelColor(QColor(Qt::red));
    plot->legend->setVisible(true); // Enable Legend
    // Set the legend in the upper left corner of the chart
    plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft |
                                                              Qt::AlignTop);
    plot->yAxis->setRange(0, Max);
    plot->yAxis2->setRange(0, Max);

    plot->legend->setIconSize(20, 20);
    plot->legend->setFont(QFont(QFont().family(), legend_font_size));
}
void Plot::SetPen(int width)
{
    QPen *pen = new QPen();
    pen->setWidth(width);
    for (int i = 0; i < graphs_count; i++) {
        graphs[i]->graphic->setPen(*pen);
    }
}
void Plot::AddGraph(QString title, QColor clr, QCPGraph::LineStyle ls,
                    int width)
{
    graphs.push_back(new Graphic(title, clr, ip));
    graphs_count++;

    auto &graph = graphs.back()->graphic;
    graph       = new QCPGraph(plot->xAxis, plot->yAxis);
    plot->addPlottable(graph);
    graph->setName(
        graphs.back()->line +
        (graphs.back()->line.indexOf("stats.mem") != -1 ? " Mi" : " %"));

    QPen pen(clr);
    pen.setWidth(width);
    graph->setPen(pen);

    graph->setAntialiased(false);
    if (history) {
        graphs.back()->SetHistory();
    }

    SetRange();

    graph->setLineStyle(ls); // Charts in a pulse ticks view
}

void Plot::Update()
{
    for (int i = 0; i < graphs_count; i++) {
        graphs[i]->Update();
    }
}
int Plot::CheckFirst()
{
    if (first != 0) {
        return 1;
    } else {
        for (int i = 0; i < graphs_count; i++) {
            if (graphs[i]->Check() != -1) {
                first = static_cast<long long>(graphs[i]->nextKey);
                break;
            }
        }
        for (int i = 0; i < graphs_count; i++) {
            graphs[i]->first = first;
        }
    }
    if (first != 0) {
        return 1;
    } else
        return -1;
}
void Plot::SetData()
{
    for (int i = 0; i < graphs_count; i++) {
        graphs[i]->SetData();
    }
}

int Plot::Check()
{
    for (int i = 0; i < graphs_count; i++) {
        if (graphs[i]->Check() != -1)
            return 1;
    }
    return -1;
}
void Plot::SetRange()
{
    for (int i = 0; i < graphs.size(); i++) {
        if (graphs[i]->nextKey > max_key)
            max_key = static_cast<int>(graphs[i]->nextKey);
    }
    plot->xAxis->setRange(max_key - interval, max_key + padding_right);
}
Plot::~Plot()
{
    for (int i = 0; i < graphs_count; i++) {
        delete graphs[i];
    }
    delete plot;
}
