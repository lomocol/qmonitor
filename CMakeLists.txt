cmake_minimum_required(VERSION 3.5)

project(CMakeTest LANGUAGES CXX)
add_subdirectory(hiredis)
#add_subdirectory(qcustomplot)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
#set(CMAKE_PREFIX_PATH /home/sp/QtMingw/static)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
#set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Widgets REQUIRED)
find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
#./configure -xplatform win32-g++ -device-option CROSS_COMPILE=/usr/bin/i686-w64-mingw32- -prefix /home/sp/foobar/mingw_64 -opensource -no-compile-examples -opengl desktop-skip qtactiveqt -skip qtcharts -skip qtdoc -skip qtlocation -skip qtremoteobjects -skip qtserialbus -skip qtwebchannel -skip qtwebview -skip qtandroidextras -skip qtconnectivity -skip qtgamepad -skip qtmacextras -skip qtpurchasing -skip qtscript -skip qttranslations -skip qtwebengine -skip qtwinextras -skip qtdatavis3d -skip qtgraphicaleffects -skip qtmultimedia -skip qtquickcontrols -skip qtscxml -skip qtspeech -skip qtvirtualkeyboard -skip qtwebglplugin -skip qtx11extras -skip qt3d -skip qtcanvas3d -skip qtdeclarative -skip qtimageformats -skip qtnetworkauth -skip qtquickcontrols2 -skip qtsensors -skip qtwayland -skip qtwebsockets

find_package(Qt5PrintSupport REQUIRED)

#QMAKE_LFLAGS += -no-pie

add_executable(CMakeTest
  main.cpp
  mainwindow.cpp
  mainwindow.h
  mainwindow.ui

  hiredis/adapters/qt.h
  qcustomplot.cpp
  qcustomplot.h
  commander.h
  commander.cpp
  graphic.h
  graphic.cpp
  manager.h
  manager.cpp
  plot.h
  plot.cpp
  table.h
  table.cpp
)

target_link_libraries(CMakeTest PRIVATE Qt5::Widgets hiredis qcustomplot Qt5::PrintSupport)
