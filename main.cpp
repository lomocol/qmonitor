#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QCoreApplication>
#include <QDebug>
#include <QTableWidget>
#include <vector>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow w(nullptr);
    w.show();
    return app.exec();
}
