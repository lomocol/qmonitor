#ifndef MANAGER_H
#define MANAGER_H
#include "mainwindow.h"

class M
{
public:
    M()
    {
    }
};
class Manager
{
public:
    Manager();
    Manager(const char *host, int port);
    int SendComand(QString command);
    redisReply *reply;
    QString result = "";

private:
    redisContext *connect;
    void *pointer;
    QByteArray bytes;
};

#endif // MANAGER_H
