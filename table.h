#ifndef TABLE_H
#define TABLE_H

#include "commander.h"
#include <QMainWindow>
#include <qcustomplot.h>

class Table
{
public:
    ~Table();
    Table(QTableWidget *table, QString _ip);
    void Upgrade();

private:
    void Restructure();
    void SetItems();
    QTableWidget *table;
    QString ip;
    QVector<QString> keys;
    QVector<QTableWidgetItem *> items;
    Commander *commander;
};

#endif // TABLE_H
