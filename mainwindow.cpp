#include "mainwindow.h"

extern const int line_width;
extern const int table_update_time;
extern const int updatatime;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    color = {Qt::green,      Qt::red,       Qt::blue,      Qt::magenta,
             Qt::darkYellow, Qt::yellow,    Qt::DownArrow, Qt::darkMagenta,
             Qt::darkCyan,   Qt::darkGreen, Qt::darkRed,   Qt::darkBlue,
             Qt::darkGray};

    layouts.push_back(ui->gridLayout_8);
    layouts.push_back(ui->gridLayout_3);
    layouts.push_back(ui->gridLayout_4);
    layouts.push_back(ui->gridLayout_5);

//    ui->lineEdit->setText("10.0.2.");
//    ui->lineEdit->setText("127.0.0.");
         ui->lineEdit->setText("localhost");
}

void MainWindow::CreateNewConnection(const QString &new_ip)
{
    ip = new_ip;

    commander = new Commander(ip);
    LoadKeys();
    CreatePlots();

    for (int i = 0; i < layouts.size(); i++) {
        layouts[i]->addWidget(plots[i]->plot, 0, 0, 0, 0);
    }

    table = new Table(ui->tableWidget, ip);

    ConnectDoubleClick();
    QTimer::singleShot(updatatime, this, SLOT(PlotTick()));
    QTimer::singleShot(table_update_time, this, SLOT(TableTick()));
}

void MainWindow::TableTick()
{
    table->Upgrade();
    QTimer::singleShot(table_update_time, this, SLOT(TableTick()));
}

void MainWindow::CreatePlots()
{
    for (int i = 0; i < keys.size(); i++) {

        if (keys[i].startsWith("stats.cpu.cpu"))
            cpu_keys.push_back(keys[i]);

        else if (keys[i].indexOf("stats.cpu") != -1)
            cpu_appl_keys.push_back(keys[i]);

        else if (keys[i].indexOf("stats.mem.total") != -1)
            mem_total_keys.push_back(keys[i]);

        else if (keys[i].indexOf("stats.mem") != -1)
            mem_appl_keys.push_back(keys[i]);
    }
    plots.push_back(new Plot(ip, MaxCPU, true));
    for (int i = 0; i < cpu_keys.size(); i++) {
        plots[0]->AddGraph(cpu_keys[i], color[i], QCPGraph::lsLine, line_width);
    }
    plots.push_back(new Plot(ip, MaxMem, true));
    for (int i = 0; i < mem_total_keys.size(); i++) {
        plots[1]->AddGraph(mem_total_keys[i], color[i], QCPGraph::lsLine,
                           line_width);
    }
    plots.push_back(new Plot(ip, MaxCPU, true));
    for (int i = 0; i < cpu_appl_keys.size(); i++) {
        plots[2]->AddGraph(cpu_appl_keys[i], color[i], QCPGraph::lsLine,
                           line_width);
    }
    plots.push_back(new Plot(ip, MaxMem, true));
    for (int i = 0; i < mem_appl_keys.size(); i++) {
        plots[3]->AddGraph(mem_appl_keys[i], color[i], QCPGraph::lsLine,
                           line_width);
    }
}

void MainWindow::LoadKeys()
{
    QString command = "keys st*";
    keys            = commander->SendCommand(command);
}

void MainWindow::CheckFirst()
{
    for (int i = 0; i < plots.size(); i++) {
        plots[i]->CheckFirst();
    }
}

void MainWindow::PlotTick()
{
    for (int i = 0; i < plots.size(); i++) {
        if (plots[i]->Check() != -1) {
            if (plots[i]->first != 0) {
                plots[i]->Update();
                plots[i]->SetData();
                if (plots[i]->real_time)
                    plots[i]->SetRange();
                plots[i]->plot->replot();
            } else
                plots[i]->CheckFirst();
        }
    }
    QTimer::singleShot(updatatime, this, SLOT(PlotTick()));
}

void MainWindow::Draw()
{
    foreach (auto plot, plots) {
        plot->SetData();
        plot->SetRange();
        plot->plot->replot();
    }
}

void MainWindow::SetPen(int width)
{
    for (int i = 0; i < plots.size(); i++) {
        plots[i]->SetPen(width);
    }
}

void MainWindow::on_RealTimeButton_1_clicked()
{

    for (int i = 0; i < (plots.size() >= 2 ? 2 : plots.size()); i++) {
        plots[i]->real_time = true;
        plots[i]->SetData();
        plots[i]->SetRange();
        plots[i]->plot->replot();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_RealTimeButton_2_clicked()
{
    for (int i = 2; i < plots.size(); i++) {
        plots[i]->real_time = true;
        plots[i]->SetData();
        plots[i]->SetRange();
        plots[i]->plot->replot();
    }
}
void MainWindow::on_SetIpButton_clicked()
{
    QString new_ip = ui->lineEdit->text();
    Commander new_commander(new_ip);
    new_commander.SendCommand("zrange stats.cpu.cpu2 0 -1");
    if (new_commander.err == 0) {
        if (ip != "") {
            DeleteOldConnection();
        }
        ip = new_ip;
        CreateNewConnection(ip);
        ui->label->setText("ip");
        ui->label->setStyleSheet("QLabel { color : green; }");
    } else {
        ui->label->setText("Wrong ip! " + new_commander.errstr);
        ui->label->setStyleSheet("QLabel { color : red; }");
    }
}
void MainWindow::ConnectDoubleClick()
{
    for (int i = 0; i < plots.size(); ++i) {
        QObject::connect(plots[i]->plot,
                         SIGNAL(mouseDoubleClick(QMouseEvent *)), this,
                         SLOT(ChangeRealTime()));
    }
}

void MainWindow::ChangeRealTime()
{
    QCustomPlot *qcplot = qobject_cast<QCustomPlot *>(sender());
    for (int i = 0; i < plots.size(); ++i) {
        if (plots[i]->plot == qcplot) {
            plots[i]->real_time = !plots[i]->real_time;
            break;
        }
    }
}
void MainWindow::DeleteOldConnection()
{
    for (int i = 0; i < layouts.size(); i++) {
        layouts[i]->removeWidget(plots[i]->plot);
        delete plots[i];
    }
    delete commander;
    delete table;

    plots.clear();
    keys.clear();
    cpu_keys.clear();
    mem_appl_keys.clear();
    cpu_appl_keys.clear();
    mem_total_keys.clear();
}
