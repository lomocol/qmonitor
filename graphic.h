#ifndef GRAPHIC_H
#define GRAPHIC_H

#include "commander.h"
#include <QMainWindow>
#include <QStringList>
#include <qcustomplot.h>

static const int line_width = 2;

static const int MaxCPU        = 100;
static const int MaxMem        = 1000;
static const int interval      = 20000; // 20'000
static const int padding_right = 1000;  // 1'000

static const int DIV              = 10;
static const int legend_font_size = 9;

static const int table_update_time = 1000;
static const int updatatime        = 100;

class Graphic
{
public:
    Graphic(const QString &str, QColor clr, const QString &ip);
    ~Graphic();
    QCPGraph *graphic;
    QString line;
    double nextKey;
    double nextVal;
    long long int first;
    void SetData();
    int SendCommand(QString command);
    void Update();
    void SetHistory();
    int Check();

private:
    void GetHistory();
    Commander *commander;
    QColor color;
    int size;
    long long count;
    QVector<QStringList> records;
    QVector<double> values;
    QVector<double> keys;
};

#endif // GRAPHIC_H
