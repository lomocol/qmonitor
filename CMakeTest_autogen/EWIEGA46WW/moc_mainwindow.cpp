/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[18];
    char stringdata0[241];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 8), // "LoadKeys"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 11), // "CreatePlots"
QT_MOC_LITERAL(4, 33, 4), // "Draw"
QT_MOC_LITERAL(5, 38, 6), // "SetPen"
QT_MOC_LITERAL(6, 45, 5), // "width"
QT_MOC_LITERAL(7, 51, 8), // "PlotTick"
QT_MOC_LITERAL(8, 60, 19), // "CreateNewConnection"
QT_MOC_LITERAL(9, 80, 6), // "new_ip"
QT_MOC_LITERAL(10, 87, 19), // "DeleteOldConnection"
QT_MOC_LITERAL(11, 107, 18), // "ConnectDoubleClick"
QT_MOC_LITERAL(12, 126, 14), // "ChangeRealTime"
QT_MOC_LITERAL(13, 141, 9), // "TableTick"
QT_MOC_LITERAL(14, 151, 10), // "CheckFirst"
QT_MOC_LITERAL(15, 162, 22), // "on_SetIpButton_clicked"
QT_MOC_LITERAL(16, 185, 27), // "on_RealTimeButton_1_clicked"
QT_MOC_LITERAL(17, 213, 27) // "on_RealTimeButton_2_clicked"

    },
    "MainWindow\0LoadKeys\0\0CreatePlots\0Draw\0"
    "SetPen\0width\0PlotTick\0CreateNewConnection\0"
    "new_ip\0DeleteOldConnection\0"
    "ConnectDoubleClick\0ChangeRealTime\0"
    "TableTick\0CheckFirst\0on_SetIpButton_clicked\0"
    "on_RealTimeButton_1_clicked\0"
    "on_RealTimeButton_2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x0a /* Public */,
       3,    0,   85,    2, 0x0a /* Public */,
       4,    0,   86,    2, 0x0a /* Public */,
       5,    1,   87,    2, 0x0a /* Public */,
       7,    0,   90,    2, 0x08 /* Private */,
       8,    1,   91,    2, 0x08 /* Private */,
      10,    0,   94,    2, 0x08 /* Private */,
      11,    0,   95,    2, 0x08 /* Private */,
      12,    0,   96,    2, 0x08 /* Private */,
      13,    0,   97,    2, 0x08 /* Private */,
      14,    0,   98,    2, 0x08 /* Private */,
      15,    0,   99,    2, 0x08 /* Private */,
      16,    0,  100,    2, 0x08 /* Private */,
      17,    0,  101,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->LoadKeys(); break;
        case 1: _t->CreatePlots(); break;
        case 2: _t->Draw(); break;
        case 3: _t->SetPen((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->PlotTick(); break;
        case 5: _t->CreateNewConnection((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->DeleteOldConnection(); break;
        case 7: _t->ConnectDoubleClick(); break;
        case 8: _t->ChangeRealTime(); break;
        case 9: _t->TableTick(); break;
        case 10: _t->CheckFirst(); break;
        case 11: _t->on_SetIpButton_clicked(); break;
        case 12: _t->on_RealTimeButton_1_clicked(); break;
        case 13: _t->on_RealTimeButton_2_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
