#ifndef COMMANDER_H
#define COMMANDER_H
#include <QMainWindow>

extern "C" {
#include <hiredis/hiredis.h>
}

class Commander
{
public:
    Commander(QString _ip);
    ~Commander();
    QString ip;
    QVector<QString> &SendCommand(const QString &_command);
    long long Check(const QString &command);
    int GetHits(const QString &command);
    int err;
    QString errstr;

private:
    redisContext *connect;
    QVector<QString> response;
    QByteArray bytes;
    redisReply *reply;
    QVector<QStringList> records;
};

#endif // COMMANDER_H
